﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class StoreDifficultyToggleValue : MonoBehaviour
{
    [SerializeField] public Difficulty difficulty;
}

