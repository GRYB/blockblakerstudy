﻿using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class AddressableSpriteLoader : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] AssetReferenceSprite assetSpriteReference;
    [SerializeField] bool loadSprite = false;
    private void Start()
    {
        if (loadSprite)
        {
            StartCoroutine(LoadSpriteAssetAsync());
        }
    }
    IEnumerator LoadSpriteAssetAsync() {
       assetSpriteReference.LoadAssetAsync().Completed += OnSpriteLoaded;
       yield return null;
    }

    void OnSpriteLoaded(AsyncOperationHandle<Sprite> obj)
    {
        if(obj.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log("Sprite load failed");
        }
        if(obj.Status == AsyncOperationStatus.Succeeded)
        {
            image.sprite = obj.Result;
        }
    }


}
