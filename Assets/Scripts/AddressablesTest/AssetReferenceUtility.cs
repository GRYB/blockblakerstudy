﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AssetReferenceUtility : MonoBehaviour
{
    public AssetReference objectToLoad;
    public AssetReference accessoryObjectToLoad;
    private GameObject instantiatedObject;
    private GameObject instantiatedAccessoryObject;
    void Start()
    {
        Addressables.LoadAssetAsync<GameObject>(objectToLoad).Completed += ObjectLoadDone;
    }

    private void ObjectLoadDone(AsyncOperationHandle<GameObject> obj)
    {
       if(obj.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log("loading failed");
        }
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
            Debug.Log("loading succeded");
            GameObject objectLoaded = obj.Result;
            instantiatedObject = Instantiate(objectLoaded);
        }
    }
}
