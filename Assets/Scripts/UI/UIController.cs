﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] Text scoreText;
    [SerializeField] Text healthText;

    void Start()
    {
        GameManager.Instance.currentSession.OnHealthUpdated += UpdateHealth;
        GameManager.Instance.currentSession.OnScoreUpdated += UpdateScore;
    }
    void UpdateScore(int score)
    {
        scoreText.text = score.ToString();
    }
    void UpdateHealth(int health)
    {
        healthText.text = health.ToString();
    }

   void OnDestroy()
    {
         GameManager.Instance.currentSession.OnHealthUpdated -= UpdateHealth;
         GameManager.Instance.currentSession.OnScoreUpdated -= UpdateScore;
    }
}
