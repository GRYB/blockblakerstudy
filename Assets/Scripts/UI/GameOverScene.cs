﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScene : MonoBehaviour
{
    [SerializeField] Text score;
    // Start is called before the first frame update
    void Start()
    {
        score.text = GameManager.Instance.currentSession.score.ToString();
    }
}
