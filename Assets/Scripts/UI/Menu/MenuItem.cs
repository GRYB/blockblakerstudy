﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuItem : MonoBehaviour
{   
    [SerializeField] MenuItemGroup group;
    private void OnValidate()
    {
        SetupGroup();
    }
    void SetupGroup()
    {
        if (group != null && !group.menuItems.Contains(this))
        {
            group.AddItem(this);
        }
    }
    public void SelectMenuItem()
    {
        if (group)
        {
            group.SetActiveItem(this);
        }
    }
}
