﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MenuItemGroup : MonoBehaviour
{
    public List<MenuItem> menuItems = new List<MenuItem>();
    public void AddItem(MenuItem item)
    {
        menuItems.Add(item);
    }
    public void SetActiveItem(MenuItem item)
    {
        foreach (MenuItem menuItem in menuItems) {
            menuItem.gameObject.SetActive(false);
        }
        if (menuItems.Contains(item)) {
            item.gameObject.SetActive(true);
        }
    }
}
