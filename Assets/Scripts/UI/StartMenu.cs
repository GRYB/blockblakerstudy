﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenu : MonoBehaviour
{
    public void StartGame()
    {
        SceneLoader.LoadScene(1);
    }
    public void QuitGame()
    {
        GameManager.Instance.QuitGame();
    }
    public void RestartGame()
    {
        GameManager.Instance.RestartGame();
    }
}
