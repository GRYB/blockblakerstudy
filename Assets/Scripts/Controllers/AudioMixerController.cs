﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioMixerController : MonoBehaviour
{
    [SerializeField] AudioMixer audioMixer;
    [SerializeField] AudioMixerSnapshot normal;
    [SerializeField] AudioMixerSnapshot lowpass;

    bool muted = false;

    private void Start()
    {
        GameManager.Instance.gameSettings.OnSettingsMenuOpen += OnMenuOpen;
        GameManager.Instance.gameSettings.OnSettingsMenuClosed += OnMenuClose;
    }
    
    public void SetFxVolume(float value)
    {
        audioMixer.SetFloat("fxVolume", Mathf.Lerp(-80, 0, value));
    }
    public void SetMusicVolume(float value)
    {
        audioMixer.SetFloat("musicVolume", Mathf.Lerp(-80, 0, value));
    }
    public void ToggleMuteSound(bool value)
    {
        muted = value;
        float level = muted ? -80 : 0;
        audioMixer.SetFloat("masterVolume", level);
    }

    public void OnMenuOpen()
    {
        lowpass.TransitionTo(0.5f);
    }
    public void OnMenuClose()
    {
        normal.TransitionTo(0.5f);
    }
    private void OnDestroy()
    {
        GameManager.Instance.gameSettings.OnSettingsMenuOpen -= OnMenuOpen;
        GameManager.Instance.gameSettings.OnSettingsMenuClosed -= OnMenuClose;
    }

}

