﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] List<BlockController> levelBlocks;
    [SerializeField] Transform blocks;

    private void Awake()
    {
        levelBlocks.Clear();
        SubscribeToBlocks();
    }

    void SubscribeToBlocks()
    {
        foreach(Transform child in blocks)
        {
            BlockController block = child.GetComponent<BlockController>();
            if (block != null) {
                levelBlocks.Add(block);
            }
        }

        if(levelBlocks.Count != 0)
        {
            foreach (BlockController block in levelBlocks) {
                block. OnBlockDestroyed += BlockDestroyed;
            } 
        }
    }

    void BlockDestroyed(BlockController block)
    {
        levelBlocks.Remove(block);
        if(levelBlocks.Count == 0)
        {
            CompleteLevel();
        }
    }

    void CompleteLevel()
    {
        NextLevel();
        //if next level available NextLevel();

    }
    void NextLevel()
    {
        SceneLoader.LoadNextScene();
    }

}
