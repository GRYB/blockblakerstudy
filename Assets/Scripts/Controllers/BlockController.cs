﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct BlockHealthSetting {
    public int health;
    public Sprite sprite;
}
public class BlockController : MonoBehaviour
{
    [SerializeField] SpriteRenderer sprite;
    [SerializeField] public BlocksData data;
    [SerializeField] AudioClip destroySound;
    Dictionary<int, Sprite> healthSprites = new Dictionary<int, Sprite>();
    int health;
    /*    public delegate void OnBlockDestroyed(BlockController blockDestroyed);
        public event OnBlockDestroyed blockDestroyed;*/
    public Action<BlockController> OnBlockDestroyed;
    public Action<BlockController> OnBlockHit;

    void Start()
    {
        Setup();
        UpdateSprite();
    }
    void Setup()
    {
        health = data.initHealth;
        foreach(BlockHealthSetting setting in data.blockHealthSettings)
        {
            healthSprites.Add(setting.health, setting.sprite);
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Ball>() != null)
        {
            GetHit();
        }
    }
    void UpdateSprite()
    {
        if (healthSprites.ContainsKey(health))
        {
            sprite.sprite = healthSprites[health];
        }
    }
    void GetHit()
    {
        health--;
        GameManager.Instance.currentSession.UpdateScore(data.pointsPerBlockHit);
        if (health == 0)
        {
            DestroyBlock();
        }
        UpdateSprite();
    }
    
    void DestroyBlock()
    {
        AudioSource.PlayClipAtPoint(destroySound, transform.position);
        GameManager.Instance.currentSession.UpdateScore(data.pointsPerBlockDestroyed);
        OnBlockDestroyed(this);
        Destroy(gameObject);
    }
}
