﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseArea : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collider) { 
        if(collider.GetComponent<Ball>() != null)
        {
            GameManager.Instance.currentSession.UpdateHealth (-1);
        }
    }
}
