﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleMovementController : MonoBehaviour
{
    [SerializeField] float minXPos;
    [SerializeField] float maxXPos;
    [SerializeField] Transform transform;
    [SerializeField] Ball ball;
    [SerializeField] float ballLaunchVelX = 20f ;
    bool ballGrabed = false;
    // Update is called once per frame


    void Start()
    {
        GrabBall();
    }
    void Update()
    {
        Vector3 mousePositionWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float x = Mathf.Clamp(mousePositionWorld.x, minXPos, maxXPos);
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
        if (Input.GetKeyDown(KeyCode.G))
        {
            Debug.Log("Grab");
            GrabBall();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("Fire");
            FireBall();
        }
    }

    void GrabBall() {
        ball.transform.position = transform.position + new Vector3(0, 0.4f,0);
        ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        ball.transform.SetParent(transform);
        ballGrabed = true;
    }
    void FireBall()
    {
        if (ballGrabed)
        {
            ball.transform.SetParent(transform.parent);
            ball.GetComponent<Rigidbody2D>().velocity = Vector2.up * ballLaunchVelX;
            ballGrabed = false;
        }

    }
}
