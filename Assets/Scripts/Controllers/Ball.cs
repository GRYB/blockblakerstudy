﻿using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    [Header("Settings")]
    [SerializeField] [Range (0.1f,0.5f)] float yVelocityNoise = 0.2f;
    [SerializeField] [Range(0.01f, 0.2f)] float yVelocityStuckThreshold = 0.1f;
    [SerializeField] [Range(0.1f, 0.5f)] float yVelocityMaxResult = 0.2f;
    [SerializeField] [Range(0.1f, 0.5f)] float xVelocityNoise = 0.2f;
    [SerializeField] [Range(0.01f, 0.2f)] float xVelocityStuckThreshold = 0.2f;
    [SerializeField] [Range(0.1f, 0.5f)] float xVelocityMaxResult = 0.2f;

    [Header("Refs")]
    [SerializeField] List<AudioClip> hitAudios;
    [SerializeField] AudioSource audio;
    Rigidbody2D rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        PlayHitSound();
        if (collision.gameObject.tag == "Wall")
        {
            if (rb.velocity.y < yVelocityStuckThreshold)
            {
                 AddNoiseVelocityY();
            }
            if (rb.velocity.x < xVelocityStuckThreshold)
            {
                AddNoiseVelocityX();
            }
        }
    }

    void AddNoiseVelocityY()
    {
        Debug.Log("Y");
        float y = rb.velocity.y + Random.Range(0, yVelocityNoise) > yVelocityMaxResult ? yVelocityMaxResult : Random.Range(0, yVelocityNoise);
        rb.velocity += new Vector2(0, y);
    }
    void AddNoiseVelocityX()
    {
        Debug.Log("X");

        float x = rb.velocity.x + Random.Range(0, xVelocityNoise) > xVelocityMaxResult ? xVelocityMaxResult : Random.Range(0, xVelocityNoise);
        rb.velocity += new Vector2(0, x);
    }

    void PlayHitSound()
    {
        audio.clip = hitAudios[Random.Range(0,hitAudios.Count)];
        audio.Play();
    }
}
