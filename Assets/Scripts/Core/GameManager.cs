﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] public UIController uiController;
    [SerializeField] SceneLoader sceneLoader;
    [SerializeField] public GameSettings gameSettings;
    [Header("Settings")]
    [SerializeField] public int startHelthPoints = 3;
    
    public GameSession currentSession;


    private static GameManager instance = null; 
    public static GameManager Instance { 
        get { 
            return instance; 
        } 
    }



    public void StartNewSession()
    {
        currentSession = new GameSession();
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void RestartGame()
    {
        StartNewSession();
        SceneLoader.LoadStartScene();
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != this)
            {
                Destroy(gameObject);
            }
        }
        StartNewSession();
    }
    private void Start()
    {
        currentSession.Setup();
    }
    private void OnDestroy()
    {
        if (instance == this)
        { 
            instance = null;
        }
    }


}
