﻿using System;
public class GameSession
{
    public int score = 0;
    public int health = 0;
    public Action<int> OnHealthUpdated;
    public Action<int> OnScoreUpdated;
    public Action OnGameOver;
    //int level;

    public void Setup()
    {
        health = GameManager.Instance.startHelthPoints;
        UpdateHealth(0);
    }

    public void UpdateHealth(int i)
    {
        health += i;
        if (health == 0)
        {
            SceneLoader.LoadGameOverScene();
            if (OnGameOver != null)
            {
                OnGameOver();
            }
        }
        if (OnHealthUpdated != null)
        {
            OnHealthUpdated(health);
        }
    }

    public void UpdateScore(int points)
    {
        score += points;
        if (OnScoreUpdated != null)
        {
            OnScoreUpdated(score);
        }
    }
}
