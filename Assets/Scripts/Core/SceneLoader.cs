﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public  class SceneLoader : MonoBehaviour {

    public static void LoadStartScene()
    {
        SceneManager.LoadScene(0);
    }

    public static void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public static void LoadScene(int index)
    {
        if (CheckSceneAvailable(index))
        {
            SceneManager.LoadScene(index);
        }
    }

    public static void LoadGameOverScene()
    {
        SceneManager.LoadScene("GameOver");
    }

    static bool CheckSceneAvailable(int index)
    {
        return true;
    }

}
