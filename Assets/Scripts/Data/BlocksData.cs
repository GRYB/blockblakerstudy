﻿using System.Collections.Generic;
using UnityEngine;


   [CreateAssetMenu(fileName = "BlocksData", menuName = "BlocksData", order = 0)]
    public class BlocksData : ScriptableObject
    {
        public List<BlockHealthSetting> blockHealthSettings;
        public int initHealth;
        public int pointsPerBlockDestroyed;
        public int pointsPerBlockHit;
}
