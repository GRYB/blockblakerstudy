﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerStats")]
public class PlayerStats : ScriptableObject
{
  [SerializeField] public int hp;
  [SerializeField] public int score;
  [SerializeField] public int level;
}

[System.Serializable]
public enum Difficulty{
    easy = 0,
    medium = 1,
    expert = 2
}

[CreateAssetMenu(fileName = "PlayerSettings")]
[System.Serializable]
public class PlayerSettingsSO : ScriptableObject
{
    public string userName;
    public int difficultyIndex;
    public int soundMute;
    public float musicVolume;
    public float fxVolume;
}

/*[System.Serializable]
public class PlayerSettings
{
    public string userName;
    public int difficultyIndex;
    public int soundMute;
    public float musicVolume;
    public float fxVolume;
}*/
