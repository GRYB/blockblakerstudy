﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class GameSettings : MonoBehaviour
{
    [Header("Player pref fields")]
    [SerializeField] InputField userName;
    [SerializeField] ToggleGroup difficultyToggle;
    [SerializeField] Toggle muteSound;
    [SerializeField] Slider musicVolSlider;
    [SerializeField] Slider fxVolSlider;
    [Header("Refs")]
    [SerializeField] PlayerSettingsSO playerSettings;
   // [SerializeField] PlayerSettings _playerSettings;
    [SerializeField] GameObject settingsContainer;
    public Action OnSettingsMenuOpen;
    public Action OnSettingsMenuClosed;

    bool settingsMenuOpen = false;
    private void Update()
    {
       if(Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleSettingMenu();
        }
    }

    private void ToggleSettingMenu()
    {
        settingsContainer.SetActive(!settingsContainer.activeSelf);
        settingsMenuOpen = settingsContainer.activeSelf;
        if (settingsMenuOpen)
        {
            if (OnSettingsMenuOpen != null)
            {
                OnSettingsMenuOpen();
            }
        }
        else {
            if (OnSettingsMenuClosed != null)
            {
                OnSettingsMenuClosed();
            }
        }
        
    }

    public void SavePlayerSettings() {
       
        // playerSettings.userName = Validate(userName.text) ? "user" : userName.text;
        playerSettings.userName = String.IsNullOrWhiteSpace(userName.text) ? "user" : userName.text;
        playerSettings.difficultyIndex = (int)difficultyToggle.ActiveToggles().First().GetComponent<StoreDifficultyToggleValue>().difficulty;
        playerSettings.difficultyIndex = 1;
        playerSettings.soundMute = muteSound.isOn ? 1 : 0;
        playerSettings.musicVolume = musicVolSlider.value;
        playerSettings.fxVolume = fxVolSlider.value;
        WritePrefs();
    }

    void WritePrefs()
    {
        string json = JsonUtility.ToJson(playerSettings);
        PlayerPrefs.SetString(name, json);
    }

    public void LoadplayerSettings()
    {
        JsonUtility.FromJsonOverwrite(PlayerPrefs.GetString(name), playerSettings);
    }
    private void OnDisable()
    {
        WritePrefs();
    }
    void OnEnable()
    {
        LoadplayerSettings();
    }


}
